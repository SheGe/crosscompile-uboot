#!/bin/sh

UBOOT_VERSION="${CI_UBOOT:=2021.10}"

rm -rf uboot
mkdir uboot
mkdir -p deploy
D="$(pwd)"
CORES="-j$(($(nproc) + 2))"
echo "using $CORES"

[ -e "u-boot-${UBOOT_VERSION}.tar.bz2" ] || wget http://ftp.denx.de/pub/u-boot/u-boot-${UBOOT_VERSION}.tar.bz2
if [ -f "u-boot-${UBOOT_VERSION}.tar.bz2" ]; then
  tar -xjf u-boot-${UBOOT_VERSION}.tar.bz2
  cd u-boot-${UBOOT_VERSION} || exit 1
else
  git clone https://github.com/u-boot/u-boot.git u-boot
  cd u-boot || exit 1
  git checkout ${UBOOT_VERSION} --quiet || exit 1
fi

# model a/b/zero
[ -n "$UBOOT_SILENT" ] && "$D"/patch-uboot-silent.sh rpi_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- distclean
make CROSS_COMPILE=arm-linux-gnueabi- rpi_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- "$CORES" u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi1.bin
cp u-boot.map "$D"/uboot/u-boot_rpi1.map
# model zero w
[ -n "$UBOOT_SILENT" ] && "$D"/patch-uboot-silent.sh rpi_0_w_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- distclean
make CROSS_COMPILE=arm-linux-gnueabi- rpi_0_w_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- "$CORES" u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi0_w.bin
cp u-boot.map "$D"/uboot/u-boot_rpi0_w.map
# model 2 b
[ -n "$UBOOT_SILENT" ] && "$D"/patch-uboot-silent.sh rpi_2_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- distclean
make CROSS_COMPILE=arm-linux-gnueabi- rpi_2_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- "$CORES" u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi2.bin
cp u-boot.map "$D"/uboot/u-boot_rpi2.map
# model 3 (32 bit)
[ -n "$UBOOT_SILENT" ] && "$D"/patch-uboot-silent.sh rpi_3_32b_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- distclean
make CROSS_COMPILE=arm-linux-gnueabi- rpi_3_32b_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- "$CORES" u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi3.bin
cp u-boot.map "$D"/uboot/u-boot_rpi3.map
# model 4 (32 bit)
[ -n "$UBOOT_SILENT" ] && "$D"/patch-uboot-silent.sh rpi_4_32b_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- distclean
make CROSS_COMPILE=arm-linux-gnueabi- rpi_4_32b_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- "$CORES" u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi4.bin
cp u-boot.map "$D"/uboot/u-boot_rpi4.map
# 64 bit
[ -n "$UBOOT_SILENT" ] && "$D"/patch-uboot-silent.sh rpi_arm64_defconfig
make CROSS_COMPILE=aarch64-linux-gnu- distclean
make CROSS_COMPILE=aarch64-linux-gnu- rpi_arm64_defconfig
make CROSS_COMPILE=aarch64-linux-gnu- "$CORES" u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi-64.bin
cp u-boot.map "$D"/uboot/u-boot_rpi-64.map

cd "$D"/uboot || exit 1
[ -n "$UBOOT_SILENT" ] && POSTFIX="-silent"
F="u-boot$POSTFIX-blob-$UBOOT_VERSION.tar.bz2"
tar -cvjf ../deploy/"$F" ./*.bin ./*.map
cd ../ || exit 1
echo "$F" >>deploy/name
echo "$UBOOT_VERSION" >deploy/version
